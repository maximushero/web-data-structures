package com.company;

public class Sorting {
    private int[] array;
    public Sorting(int size){
        array = new int[size];
        for (int i = 0; i < array.length; i++){
            array[i] = (int) Math.round(Math.random() * 1000);
        }
    }

    public void Insertion_sort(){
        int j, temp;
        for (int i = 0; i < array.length; i++){
            temp = array[i];
            for (j = i - 1; j >= 0 && array[j] > temp; j--){
                array[j+1] = array[j];
            }
            array[j+1] = temp;
        }
    }

    public void Selection_sort(){
        int min, temp;
        for (int i = 0; i < array.length-1; i++){
            min = i;
            for (int j = i+1; j < array.length; j++){
                if (array[j] < array[min])
                    min = j;
            }

            temp = array[min];
            array[min] = array[i];
            array[i] = temp;
        }
    }

    public void PrintArray(){
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

}
