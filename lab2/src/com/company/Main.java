package com.company;

public class Main {

    public static void main(String[] args) {

        Sorting Array = new Sorting(2500);

        Array.PrintArray();

        long start = System.nanoTime();
       // Array.Selection_sort();
        Array.Insertion_sort();
        long end = System.nanoTime();
        long time = end - start;
        System.out.println("Время сортировки в секундах: " + time * Math.pow(10, -9));

        Array.PrintArray();



    }
}
