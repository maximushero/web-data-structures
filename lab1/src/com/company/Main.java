package com.company;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
	    Massiv_Test Test = new Massiv_Test(15); //Выделяю место в памяти

        for (int i = 0; i < 15; i++){
            Test.insert(i); // Вставляю элементы в массив
        }

        Test.display(); //Заполненный массив

        if (Test.find(5)){  // Поиск элемента в массиве
            System.out.println("Элемент есть в массиве");
        }
        else {
            System.out.println("Элемента нет в массиве");
        }

        if (Test.delete(10)){  // Удаление элемента в массиве
            System.out.println("Элемент удален");
        }
        else {
            System.out.println("Элемента не найден");
        }

        Test.display(); //Показываю, что элемент удален

    }

}

